/* eslint-disable */
import Vue from "vue";
import App from "./App.vue";
import router from "../router";
import "./registerServiceWorker";
import ArgonDashboard from "./plugins/argon-dashboard";
import axios from "axios";
import moment from "moment";
import VueSocketIO from "vue-socket.io-extended";
import io from "socket.io-client";
import VueMeta from "vue-meta";
import VueScrollTo from "vue-scrollto";
import PapaParse from "papaparse";
import jwt from "jsonwebtoken"

console.log(process.env.VUE_APP_MY_ENV);
Vue.prototype.$mysocketserver = process.env.VUE_APP_SOCKET_SERVER;

Vue.config.productionTip = false;
Vue.prototype.$http = axios;
Vue.prototype.$moment = moment;
Vue.prototype.$papa = PapaParse;
Vue.prototype.$jwt = jwt;

Vue.use(ArgonDashboard);
Vue.use(VueMeta);
Vue.use(VueScrollTo);

const socket = io(Vue.prototype.$mysocketserver);
Vue.use(VueSocketIO, socket);

Vue.mixin({
  data: function() {
    return {
      globalUserPriviledges: ""
    };
  },
  methods: {
    globalSetupUsersPriviledge() {
      this.$http
        .get("http://localhost:3000/getRolePriviledges")
        .then(response => {
          let data = response.data;
          let userRoleId = JSON.parse(localStorage.getItem("user")).userRole;
          this.globalUserPriviledges = data.filter(item => {
            return item.roleId == userRoleId;
          });
          console.log("View User priviledges");
          console.log(this.globalUserPriviledges);
        })
        .catch(err => {
          console.error(err);
        });
    },
    camelize(str) {
      return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function(match, index) {
        if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
        return index == 0 ? match.toLowerCase() : match.toUpperCase();
      });
    },
    formatNumber(num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }
  }
});

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
