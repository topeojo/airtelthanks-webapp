import Vue from "vue";
import Router from "vue-router";
import DashboardLayout from "@/layout/DashboardLayout";
import AuthLayout from "@/layout/AuthLayout";

import Login from "@/views/Login";

import LandingPage from "@/views/LandingPage";
import NotFoundPage from "@/views/NotFoundPage";
import HomePage from "@/views/HomePage";
import Dashboard from "@/views/Dashboard";
import GiftUsers from "@/views/GiftUsers";

Vue.use(Router);

let router = new Router({
  linkExactActiveClass: "active",
  mode: "history",
  routes: [
    {
      path: "/",
      name: "homepage",
      component: HomePage
    },
    {
      path: "/entry",
      redirect: "login",
      component: AuthLayout,
      children: [
        {
          path: "/login",
          name: "login",
          component: Login
        },
        {
          path: "/services",
          component: LandingPage,
          meta: {
            requiresAuth: true
          }
        }
      ]
    },
    {
      path: "/board",
      name: "board",
      redirect: "dashboard",
      component: DashboardLayout,
      meta: {
        requiresAuth: true
      },
      children: [
        {
          path: "/dashboard",
          name: "dashboard",
          component: Dashboard
        },
        {
          path: "/giftusers",
          name: "Gift Users",
          component: GiftUsers
        }
      ]
    },
    {
      path: "*",
      name: "404",
      component: NotFoundPage
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem("airtelthankstoken") == null) {
      next({
        path: "/login"
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
