module.exports = {
  handle(data) {
    console.log("bulk gifting processing...");
    return data;
  },

  before(data) {
    console.log("bulk gifting before()...");
  },

  done(response, data) {
    console.log("bulk gifting done()...", response);
  }
};
