const Bulkgifting = require("./Bulkgifting");

const PROCESSORS = {
  BULKGIFTING: "Bulkgifting"
};

const IProcess = function(processorType) {
  const defaults = {
    process(data) {},

    before(data) {
      console.log("before hook...", data);
    },

    done(response, data) {
      console.log("done hook", response, data);
    }
  };

  let processor = null;
  switch (processorType) {
    case PROCESSORS.BULKGIFTING:
      processor = Bulkgifting;
      console.log(Bulkgifting);
      break;
    default:
      throw new Error("Unknown proessor ".processorType);
  }

  processor.before = processor.before || defaults.before;
  processor.done = processor.done || defaults.done;

  const process = processor.handle || false;
  if (!process) {
    throw new Error("Something is wrong");
  }

  processor.process = function(data) {
    return new Promise(resolve => {
      Promise.all([processor.before(data), processor.handle(data)]).then(
        ([first, second]) => {
          processor.done(second, data);
          resolve(second);
        }
      );
    });
  };

  return processor;
};

IProcess.PROCESSORS = PROCESSORS;

module.exports = IProcess;
