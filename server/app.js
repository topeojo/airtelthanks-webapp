"use strict";
const express = require("express");
const path = require("path");
const jwt = require("jsonwebtoken");
const bodyParser = require("body-parser");
const cors = require("cors");
const helmet = require("helmet");
const { redisClient } = require("./config/redisConnectConfig");
const axios = require("axios");
const otpGen = require("otp-generator");
const history = require("connect-history-api-fallback");
const { App: EnvApp } = require("./config/index");
const { mailSender } = require("./config/nodemailer");
const writeFile = require("write-file-bluebird");
const CashToken = require("./config/cashtokenConfigs");
const IProcessors = require("./processors");

let airtelUploadsFolderName =
  EnvApp.REDIS_ENV === "local"
    ? "devAirtelThanksUploads"
    : "airtelThanksUploads";

// global.loginPhone = "09065961820";
// global.loginPass = "11223344";

const API_URL = "https://jsonplaceholder.typicode.com";

const app = express();

app.use(history());
app.use(express.static(path.resolve(__dirname, "dist")));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.use(bodyParser.json({ limit: "50mb", extended: true }));
app.use(cors());
app.use(helmet());

app.get("/getCashTokenBalance", (req, res) => {
  CashToken.getCashTokenBalance().then(resp => {
    // console.log(resp.data);
    res.status(200).send({ cashTokenBalance: resp.data.balance });
  });
});

app.get("/getTokensGiftedOut", (req, res) => {
  CashToken.makeAuthRequest("celdmobile/getDashboardData", {
    userId: CashToken.user.userId,
    accessToken: CashToken.user.accessToken
  }).then(resp => {
    console.log("Getting response for dashboard");
    // console.log(resp.data);
    res.status(200).send({
      cashTokensGiftedOut: resp.data.merchant.totalTokensGiftedOut
    });
  });
});

app.post("/getFileRecords", function(req, res) {
  // console.log(req.body);

  const handler = IProcessors(IProcessors.PROCESSORS.BULKGIFTING);
  console.log("**************", { handler, IProcessors });
  // return res.sendStatus(200);
  return handler
    .process(req.body.cleanRecords)
    .then(rdata => {
      res.json(rdata);
    })
    .catch(error => {
      console.error(error);
      res.sendStatus(500);
    });
  // return;

  let message = `Good Day.\n\nYour cashtoken file upload was successful. Please find the details of the upload below:\n\nUpload file name: ${req.body.fileName},\nTotal records in the file: ${req.body.totalRecordsInFile},\nClean records (records that passed the integrity test): ${req.body.cleanRecords.length},\nFaulty records (records that failed the integrity test): ${req.body.faultyRecords.length}\nList of faulty records:\n`;

  req.body.faultyRecords.forEach(value => {
    message = message + JSON.stringify(value) + "\n";
  });

  mailSender(
    "etopeojo@gmail.com",
    [`${req.body.email}`],
    "Cashtoken File Upload Successful!",
    message,
    (error, info) => {
      if (error) {
        throw error;
      }
      console.log("Processed records email sent successfully");
    }
  );
  /* 
  if (Array.isArray(req.body.cleanRecords)) {
    let clean = req.body.cleanRecords;
    clean.forEach(({ phone_number, tokensQty }) => {
      CashToken.giftTokens(phone_number, tokensQty, "Gifting for Airtel Demo")
        .then(giftingResponse => {
          console.log("Response: ", giftingResponse.data.message);
        })
        .catch(console.error);
    });
    // CashToken.blastTokens(clean).then(console.log);
  } */

  redisClient
    .incrAsync("airtelThanksUploadCount")
    .then(resp => {
      let batchID = `airtelThanks_00${resp}`;
      return batchID;
    })
    .then(resp => {
      let file = path.join(
        __dirname,
        `${airtelUploadsFolderName}`,
        `${resp}.json`
      );
      let stringifiedCleanRecords = JSON.stringify(req.body.cleanRecords);
      writeFile(file, stringifiedCleanRecords).then(() => {
        let clean = req.body.cleanRecords;
        clean.forEach(({ phone_number, tokensQty }) => {});
      });
    });

  let finishedMessage = `Hello ${req.body.email}\n\nThe cashtoken giftings are completed now, and you can login to your dashboard for more details.\n\nRegards. `;
  mailSender(
    "etopeojo@gmail.com",
    [`${req.body.email}`],
    "Cashtoken Gifting Completed Successful!",
    finishedMessage,
    (error, info) => {
      if (error) {
        throw error;
      }
      console.log("Finished records gifting email sent successfully");
    }
  );
  return res.status(200).send("Upload completed successfully!");

  //

  // res.status(200).send("Upload completed successfully!");
});

app.post("/confirmLoginDetails", (req, res) => {
  redisClient.exists(`user:${req.body.email}`, (err, result) => {
    if (err) {
      console.log(err);
      return res.status(500).send("Error getting user from DB");
    }

    if (result === 0) {
      console.log(`User ${req.body.email} trying to log in does not exist`);
      return res.status(401).send("Error User does not exist");
    } else if (result === 1) {
      redisClient.hget(`user:${req.body.email}`, "password", (err, result) => {
        if (err) {
          console.log(err);
          return res.status(500).send("Error getting user from DB");
        }

        if (result === req.body.password) {
          console.log(
            `User ${req.body.email} first step login successful, next up... OTP!`
          );
          let generatedOTP = otpGen.generate(8, { specialChars: false });
          redisClient.setex(
            `otp:${req.body.email}:${generatedOTP}`,
            300,
            "true",
            (err, reply) => {
              if (err) {
                console.log(err);
                return res.status(500).send("Error generating OTP");
              }
              let message = `Hello ${req.body.email},\n\nYour login OTP is ${generatedOTP}\nPlease be advised that this OTP will expire in 5 minutes\n\nRegards.`;

              mailSender(
                "etopeojo@gmail",
                [`${req.body.email}`],
                "AirtelThanks Login OTP",
                message,
                (error, info) => {
                  if (error) {
                    throw error;
                  }
                  console.log("OTP email sent successfully");
                }
              );
              return res.status(200).send("Success Password");
            }
          );
        } else {
          console.log(
            `User ${req.body.email} first step authentication failed`
          );
          return res.status(401).send("Error User Authentication failed");
        }
      });
    }
  });
});

app.post("/confirmOTP", (req, res) => {
  redisClient.exists(`otp:${req.body.email}:${req.body.otp}`, (err, result) => {
    if (err) {
      console.log(err);
      return res.status(500).send("Error getting user from DB");
    }

    if (result === 0) {
      console.log(
        `User ${req.body.email} trying to log in with non-existent OTP`
      );
      return res.status(401).send("Error User does not exist");
    } else if (result === 1) {
      console.log(
        `User ${req.body.email} completed 2 step Auth and logged in successfully`
      );
      let token = jwt.sign({ email: req.body.email }, EnvApp.JWTKEY);
      redisClient.del(`otp:${req.body.email}:${req.body.otp}`, (err, resp) => {
        if (resp == 1) {
          console.log(
            `OTP created for ${req.body.email} deleted successfuly on user login`
          );
        }
      });
      return res.status(200).send({
        airtelthankstoken: token,
        message: "Success Password and OTP"
      });
    }
  });
});

app.get("/getPosts", (req, res) => {
  let range = req.query.range;
  let start = (range - 1) * 10;
  let end = start + 9;

  redisClient.exists("posts", (err, existRes) => {
    console.log("Reply from exist == " + existRes);
    if (existRes === 0) {
      axios.get(`${API_URL}/posts`).then(response => {
        console.log("Fetching Info from API");
        for (let post of response.data) {
          redisClient.rpush("posts", JSON.stringify(post));
        }

        redisClient.expire("posts", 180);

        redisClient.lrange("posts", start, end, (err, reply) => {
          if (err) throw err;
          let responseObj = [];
          let responseKeys = [];

          for (let post of reply) {
            let postObj = JSON.parse(post);
            responseObj.push(postObj);
          }
          for (let key in responseObj[0]) {
            responseKeys.push(key);
          }

          return res
            .status(200)
            .send({ dataPosts: responseObj, dataKeys: responseKeys });
        });
      });
    } else if (existRes === 1) {
      console.log("Fetching Info from Redis");
      redisClient.lrange("posts", start, end, (err, reply) => {
        if (err) throw err;
        let responseObj = [];
        let responseKeys = [];

        for (let post of reply) {
          let postObj = JSON.parse(post);
          responseObj.push(postObj);
        }
        for (let key in responseObj[0]) {
          responseKeys.push(key);
        }
        return res
          .status(200)
          .send({ dataPosts: responseObj, dataKeys: responseKeys });
      });
    }
  });

  console.log("Done adding to redis");
});

app.get("/getMockUserDetails", (req, res) => {
  redisClient.exists("mockUserDetails", (err, result) => {
    if (result === 0) {
      console.log("Fetching Mock User Details from the DB and adding to cache");
      db.getMockUserDetailsLimited((err, data) => {
        if (err) return res.status(500).send("Error getting information");

        let responseObj = data;
        let responseKeys = [];

        for (let key in responseObj[0]) {
          responseKeys.push(key);
        }

        responseObj.forEach(value => {
          let stringedValue = JSON.stringify(value);
          redisClient.lpush("mockUserDetails", stringedValue);
        });

        return res
          .status(200)
          .send({ dataPosts: responseObj, dataKeys: responseKeys });
      });
    } else if (result === 1) {
      console.log("Fetching Mock User Details from the Cache");
      redisClient.lrange("mockUserDetails", 0, -1, (err, result) => {
        let parsedResult = result.map(value => {
          return JSON.parse(value);
        });

        let responseKeys = [];

        for (let key in parsedResult[0]) {
          responseKeys.push(key);
        }

        return res
          .status(200)
          .send({ dataPosts: parsedResult, dataKeys: responseKeys });
      });
    }
  });
});

app.post("/getMockUserDetailsQueried", (req, res) => {
  db.getMockUserDetailsDateQueried(
    [req.body.dateFrom, req.body.dateTo],
    (err, data) => {
      if (err) return res.status(500).send("Error getting information");

      let responseObj = data;
      let responseKeys = [];

      for (let key in responseObj[0]) {
        responseKeys.push(key);
      }
      return res
        .status(200)
        .send({ dataPosts: responseObj, dataKeys: responseKeys });
    }
  );
});

let port = process.env.PORT || 3000;

let server = app.listen(port, () => {
  console.log("Express server listening on port " + port);
});

/* THIS IS THE SOCKET IO SIDE OF THE SERVER */
const io = require("socket.io")(server);

io.on("connection", socket => {
  console.log("New User Connected");

  socket.on("getNewReport", data => {
    console.log("Got request to fetch updated mock user details data");
    db.getMockUserDetails((err, report) => {
      redisClient.lrange("mockUserDetails", 0, -1, (err, result) => {
        console.log(report.length);
        console.log(result.length);
        if (report.length > result.length) {
          console.log("There are new updated data from the DB");
          let newUpdatedData = report.slice(result.length);
          // console.log(newUpdatedData);

          newUpdatedData.forEach(value => {
            let stringedValue = JSON.stringify(value);
            redisClient.lpush("mockUserDetails", stringedValue);
          });

          redisClient.lrange("mockUserDetails", 0, -1, (err, result) => {
            let parsedResult = result.map(value => {
              return JSON.parse(value);
            });

            let responseKeys = [];

            for (let key in parsedResult[0]) {
              responseKeys.push(key);
            }

            console.log(parsedResult);

            setTimeout(() => {
              socket.emit("obtainNewReport", {
                dataPosts: parsedResult,
                dataKeys: responseKeys,
                checkUpdate: "new update"
              });
            }, 7000);
          });
        } else if (report.length === result.length) {
          console.log(
            "There is no new data from the DB for us to update our cache with"
          );
          setTimeout(() => {
            socket.emit("obtainNewReport", {
              checkUpdate: "no update"
            });
          }, 7000);
        }
      });
    });
  });
});
