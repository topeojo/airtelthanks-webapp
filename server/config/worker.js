var redis = require('redis');
var Promise = require("bluebird");
Promise.promisifyAll(require("redis"));

var worker = redis.createClient();

/*
    "message" (channel, message)
    Client will emit message for every message received that 
    matches an active subscription. Listeners are passed the channel 
    name as channel and the message as message.
*/

worker.on('message', (channel, message) => {
    /*
        ensure subscribe client isn't doing work unrelated to exclusively 
        listening/unsubscribing/quitting. so unsubscribe after the 
    */
    worker.unsubscribe(channel, (error, count) => {
        if (error) {
            throw new Error(error);
        }
    });
    
    worker.HGETALL(message, function(err, value) {
        if (err) (console.log(err))  
        //make an external API call to dump the object stored in value 
        //this is a simple test
        for (var property1 in value) {            
            console.log(property1);     
        }                             
    }); 
});



const channel = 'CashTokenGiftchannel';

worker.subscribe(channel, (error, count) => {
    if (error) {
        throw new Error(error);
    }
    console.log('i am listening')
});