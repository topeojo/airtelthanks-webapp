const { CashToken } = require("cashtoken-node-sdk");
const { App, CashToken: CTConfig } = require("./index");

const cashToken = new CashToken(
  CTConfig.PRIVATE_KEY,
  CTConfig.PUBLIC_KEY,
  App.PROD
).setupUserAuthentication(
  98,
  "18290055748d6052087d8e76a145c68bcc064763a0fd3c58413832b4d9723fa1",
  true
);

console.log("Done auth cashtoken");

module.exports = cashToken;
