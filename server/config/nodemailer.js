const nodemailer = require("nodemailer");
const { EmailSender } = require("./index");

function mailSender(from, to, subject, text, callback) {
  nodemailer.createTestAccount((err, account) => {
    let transporter = nodemailer.createTransport({
      host: "smtp.googlemail.com", // Gmail Host
      port: 465, // Port
      secure: true, // this is true as port is 465
      auth: {
        user: EmailSender.EMAIL, //Gmail username
        pass: EmailSender.PASSWORD // Gmail password
      }
    });

    let mailOptions = {
      from: `<${from}>`,
      to: `${to}`,
      cc: "topeojo@fisshboneandlestr.com", // Recepient email address. Multiple emails can be separated by commas
      subject: `${subject}`,
      text: `${text}`
    };

    transporter.sendMail(mailOptions, (error, info) => {
      callback(error, info);
    });
  });
}

module.exports = {
  mailSender
};
