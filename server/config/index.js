const path = require("path");

const ENV = process.env.NODE_ENV || "development";

require("dotenv").config({
  path: path.resolve(__dirname, `../.${ENV}.env`)
});

const Config = Object.freeze({
  App: {
    ENV,
    PROD: process.env.NODE_ENV === "production",
    JWTKEY: process.env.SECRET,
    REDIS_ENV: process.env.REDIS_ENV
  },
  CashToken: {
    PRIVATE_KEY: process.env.CELD_PRIVATE_KEY,
    PUBLIC_KEY: process.env.CELD_PUBLIC_KEY
  },
  EmailSender: {
    EMAIL: process.env.EMAIL,
    PASSWORD: process.env.EMAIL_PASSWORD
  }
});

module.exports = Config;
