var redis = require('redis');
var Promise = require("bluebird");
const fs = require('fs');
const writeFile = require( 'write-file-bluebird' );
var path = require( 'path' );
var file = path.join( 'c:/', 'folder', 'Mike.json' );
console.log(file);
Promise.promisifyAll(require("redis"));

var client = redis.createClient();

client.on("connect",()=>{
    console.log("Publisher is working")
})
const channelRedis = 'CashTokenGiftchannel';
const channelFileSystem = 'CashTokenGiftFileStorage';
let giftObject = {
    '09099387285': '56',
    '09099387282': '3',
    '09099387281': '4',
    '09099387283': '4',
    '09099387284': '4',
    '09099387286': '4',
    '09099387287': '4',
    '09099387288': '4',
    '09099387289': '4'
}


client.incrAsync('airtelThanks')
.then(function(res) {
    let batchID = 'airtelThanks_00'+res;
    return batchID;
}).then(function(batchID) {
    client.hmsetAsync(batchID, giftObject);
    return batchID;
}).then(function(response) {
    console.log(response)
    let pubStatus = client.publish(channelRedis, response);// create a redis hash 
    return [pubStatus, response];
}).then(function(pubResponse) {
    console.log(pubResponse)
    // save file to dropbox/googledrive
    return writeFile('C:/wamp64/www/airtelthanks'+pubResponse[1]+'.json', giftObject);// save to file  
}).then(function(data) {
    console.log('This file has been saved? '+data)
    // what do we do here?
}).catch((err)=>{
    console.log(err);
    // log this event on backoffice
    // slack channel
});